package nl.voorth.gtf.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class GtfController
{
  private static final String GTF_URL = "https://jtv.home.xs4all.nl/gtf/GPL_TLA_FAQ";

  @GetMapping(value = {"/", "/{tla}"})
  public List<String> findTla(@PathVariable(value = "tla", required = false) Optional<String> tla)
    throws IOException
  {
    var url = new URL(GTF_URL);
    var stream = url.openStream();
    var in = new InputStreamReader(stream);
    var reader = new BufferedReader(in);

    var actualTla = tla.orElse("");
    return reader
      .lines()
      .filter(l -> l.substring(0, actualTla.length()).equalsIgnoreCase(actualTla))
      .map(l -> l.substring(4))
      .collect(Collectors.toList());
  }
}
