package nl.voorth.gtf.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GtfApiApplication
{
  public static void main(String[] args)
  {
    SpringApplication.run(GtfApiApplication.class, args);
  }
}
